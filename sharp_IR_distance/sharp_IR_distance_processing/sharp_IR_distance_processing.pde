import processing.serial.*;
import java.util.List;
import java.util.Map;
import java.util.Collections;
import java.util.regex.Pattern;

Serial serial;
PrintWriter output;

List<Integer> list;
Map<Integer, Integer> probabilityMap;

Pattern pattern;

double average = 0.0;
double median = 0.0;
double deviation = 0.0;
double variance = 0.0;

long sum = 0;

static int width = 1400;
static int height = 900;

float unitX = 0.f;
int maxValue = 1;
int minValue = 1000;

float unitY = 0.f;
int maxCount = 1;

void setup() {
  size(1400, 900);
  background(0);
  pattern = Pattern.compile("^\\[\\d*\\]$");
  list = new ArrayList();
  probabilityMap = new HashMap();
  serial = new Serial(this, Serial.list()[0], 9600);
  output = createWriter("output.txt");
}

void draw() {
  if (serial.available() > 0) {
    String value = serial.readStringUntil(']');
    if (value != null && pattern.matcher(value).matches()) {
      int val = int(value.substring(1, value.length() - 1));
      println(val);
      list.add(val);
      Integer valCountHolder = probabilityMap.get(val);
      int valCount = valCountHolder == null ? 1 : valCountHolder + 1;
      probabilityMap.put(val, valCount);
      if (maxCount < valCount) {
        maxCount = valCount;
        unitY = (height - 225) / maxCount;
      }
      if (minValue > val) {
        minValue = val;
        if (maxValue != minValue) {
          unitX = (width - 125) / (maxValue - minValue);
        }
      }
      if (maxValue < val) {
        maxValue = val;
        if (maxValue != minValue) {
          unitX = (width - 125) / (maxValue - minValue);
        }
      }

      background(0);
      text("Count: " + list.size(), 10, 25);
      calculateAverage(val);
      calculateMedian();
      calculateDeviation();
      calculateVariance();
      calculateDistribution();
    }
  }
  if (list.size() == 1000.0) {
    noLoop();
    printResults();
  }
}

void printResults() {
  output.println("Average: " + average);
  output.println("Median: " + median);
  output.println("Deviation: " + deviation);
  output.println("Variance: " + variance);
  output.flush();
  output.close();
}

void calculateAverage(int val) {
  sum += val;
  average = sum / (double) list.size();
  text("Average: " + average, 10, 50);
}

void calculateMedian() {
  if (list.size() < 2) {
    return;
  }
  Collections.sort(list);
  int medIdx = list.size() / 2;
  if (list.size() % 2 == 0) {
    median = (list.get(medIdx) + list.get(medIdx - 1)) / 2.0;
  } else {
    median = list.get(medIdx + 1);
  }
  text("Median: " + median, 10, 75);
}

void calculateDeviation() {
  double sum = 0;
  for (Integer i : list) {
    sum += Math.pow(i - average, 2);
  }
  deviation = Math.sqrt(sum / list.size());
  text("Deviation: " + deviation, 10, 100);
}

void calculateVariance() {
  variance = Math.pow(deviation, 2);
  text("Variance: " + variance, 10, 125);
}

void calculateDistribution() {
  int centerX = 50;
  int centerY = height - 50;

  stroke(255);
  line(0, centerY, width, centerY); // x axis
  line(centerX, 150, centerX, height); // y axis

  for (Map.Entry<Integer, Integer> i : probabilityMap.entrySet()) {
    int value = i.getKey();
    int count = i.getValue();
    float x = centerX + 25 + ((value - minValue) * unitX);
    float y = centerY - 25 - (count * unitY);
    rect(x - 2, y - 2, 4, 4); //<>//
    text(value, x, centerY + 20);
    text(count, centerX - 20, y);
  }
}
